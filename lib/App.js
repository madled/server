/**
 * Base class for an app. Apps are run on request. They are contained by the Apps class, which controls when an app runs.
 * They have low level access to the cube to increase the speed. Each frame is a cube state, containing a Uint32Array.
 */
class App {
    constructor() {
        this.stateManager = null;
    }

    /**
     * Sets the state manager
     */
    setStateManager(stateManager) {
        this.stateManager = stateManager;
    }
}

module.exports = App;