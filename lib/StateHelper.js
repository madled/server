/**
 * StateHelper is a static class that provides some helper functions to convert from and to Uint32Array's.
 */
class StateHelper {
    /**
     * Receives a Uint32Array and converts it to an array of numbers
     * @param uint {Uint32Array}
     * @return {number[]}
     */
    static stateUintToArray(uint) {
        return Array.prototype.slice.call(uint);
    }

    /**
     * Receives a Uint32Array and converts is to JSON
     * @param uint {Uint32Array}
     * @return {string}
     */
    static stateUintToJSON(uint) {
        return JSON.stringify(StateHelper.stateUintToArray(uint));
    }

    /**
     * Receives an array of numbers (or types that can be converted to a number) and converts it to a Uint32Array
     * @param stateArray {number[]|string[]}
     * @return {Uint32Array}
     */
    static stateArrayToUint(stateArray) {
        let state = new Uint32Array(128);

        if (stateArray.constructor === Array) {
            for (let i = 0; i < stateArray.length; i++) {
                state[i] = parseInt(stateArray[i]);
            }
        }

        return state;
    }

    /**
     * Receives a pattern, an array of arrays and converts it to an array if Uint32Array's
     * @param pattern {[[]]}
     * @return {[Uint32Array]}
     */
    static patternArrayToUintArray(pattern) {
        let uintArray = [];
        for (let i = 0; i < pattern.length; i++) {
            uintArray.push(StateHelper.stateArrayToUint(pattern[i]));
        }

        return uintArray;
    }

    /**
     * Receives a uint pattern and converts it to an array of arrays
     * @param pattern {[Uint32Array]}
     */
    static patternUintArrayToArray(pattern) {
        let returnArray = [];
        for (let i = 0; i < pattern.length; i++) {
            returnArray.push(StateHelper.stateUintToArray(pattern[i]));
        }

        return returnArray;
    }
}

module.exports = StateHelper;