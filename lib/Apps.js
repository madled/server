const async = require('async');
const fs = require('fs');
const path = require('path');

const config = require('../config');

class Apps {
    constructor () {
        this.whitelist = config.apps.load || 'auto';
        this.apps = {};
    }

    loadApps() {
        let appsPath = path.join(__dirname, 'apps');

        fs.readdir(appsPath, (err, files) => {
            async.each(files, (file, eachCallback) => {
                if (file.endsWith('.js') && (this.whitelist === 'auto' || this.whitelist.indexOf(file.replace('.js', '')) !== -1)) {
                    file = path.join(appsPath, file);

                    try {
                        if (fs.lstatSync(file).isFile()) { // Only continue if the file exists
                            let App = require(file);

                            console.info(`Found app ${App.name}`);

                            this._addApp(App, () => {
                                eachCallback(); // App loaded, continue to next
                            });
                        }
                        else {
                            eachCallback(); // It's not a file, continue to next
                        }
                    }
                    catch (e) {
                        console.error(`Error while processing app file ${path.basename(file)}`);
                        console.error(e);
                    }
                }
                else {
                    eachCallback(); // File not a valid JavaScript file, continue to next
                }
            }, err => {
                if (err) {
                    console.error(err);
                }

                if (callback)
                    callback();
            });
        });
    }

    _addApp(App) {

    }
}

module.exports = Apps;