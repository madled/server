class StateManagerProxy {
    /**
     *
     * @param stateManager {StateManager}
     */
    constructor(stateManager) {
        /**
         * @type {StateManager}
         * @private
         */
        this.stateManager = stateManager;
    }

    /**
     * Set the pattern to display
     * @param patternSet {Object} The pattern description
     * @param patternSet.name {String} The name of the pattern
     * @param patternSet.speed {Number} The speed of the pattern in states-per-second
     * @param patternSet.index {Number=} The index to start the first repeat with
     * @param patternSet.startIndex {Number=} The index to start the pattern
     * @param patternSet.endIndex {Number=} The final index of the pattern to use before starting over
     * @param patternSet.repeat {Number=-1} How many times the pattern should repeat (-1 means infinitely, 0 means it will do the pattern once)
     * @param clearSequence {boolean} If the sequence should be cleared
     * @return {boolean} True of the pattern was successfully set, false otherwise
     */
    setPattern(patternSet, clearSequence = true) {
        this.stateManager.setPattern('app', patternSet, clearSequence);
    }

    /**
     * Set a sequence of patterns to go through
     * @param sequence {Object[]} The sequence of patterns as an array
     * @param sequence[].name {String} The name of the pattern
     * @param sequence[].speed {Number} The speed of the pattern in states-per-second
     * @param sequence[].index {Number=} The index to start the first repeat with
     * @param sequence[].startIndex {Number=} The index to start the pattern
     * @param sequence[].endIndex {Number=} The final index of the pattern to use before starting over
     * @param sequence[].repeat {Number=-1} How many times the pattern should repeat (-1 means infinitely, 0 means it will do the pattern once)
     * @param clear {boolean} True if the sequence needs to start from scratch
     * @return {boolean} True
     */
    setSequence(sequence, clear = true) {
        this.setSequence.setPattern('app', sequence, clear);
    }

    /**
     * Set the LED cube to a specific state.
     * This state does not get saved on file, it is a static state shown to the LED cube.
     * @param state {Uint32Array} The desired state
     */
    setState(state) {
        this.setSequence.setState('app', state);
    }
}

module.exports = StateManagerProxy;