const config = require('../config');

/**
 * The stateManager decides what data will be sent to the drivers.
 * This can be determined by either setting a pattern or setting a state.
 */
class StateManager {
    /**
     * @param drivers {Drivers}
     * @param patterns {Patterns}
     */
    constructor(drivers, patterns) {
        this.drivers = drivers;
        this.patterns = patterns;

        this.state = new Uint32Array(128);
        this.pattern = this._emptyPattern();
        this.sequence = [];

        this.interval = null;
        this.newCycle = false;
        this.lastInterval = null;
    }

    /**
     * Sets a refresh interval for displaying a pattern
     * @protected
     */
    _setPatternInterval() {
        this._clearPatternInterval();

        let speed = 1000 / Math.abs(this.pattern.speed);
        let delay = 0;
        if(this.lastInterval !== null) {
            delay = Math.max(0, speed - (new Date() - this.lastInterval)); // Calculate the amount of milliseconds gone by since last state change
        }

        setTimeout(() => {
            if (Math.round(this.pattern.speed) !== 0) { // If the speed is not 0, set a new interval
                this.interval = setInterval(() => {
                    this._nextPatternState();
                }, speed);
            }
            else {
                this.lastInterval = null;
            }
            this._nextPatternState(); // Manually cycle the next state
        }, delay);
    }

    /**
     * Clears any existing intervals for looping through the pattern
     * @protected
     */
    _clearPatternInterval() {
        if (this.interval)
            clearInterval(this.interval);

        this.newCycle = false; // Just in case
    }

    /**
     * Display the next state from a pattern
     * @protected
     */
    _nextPatternState() {
        if (this.newCycle) { // If the pattern started from the beginning again, we need to check of it may still run
            this.newCycle = false;

            if (this.pattern.repeat !== -1) { // If the pattern does NOT cycle infinitely
                let newRepeat = this.pattern.repeat - 1;

                if (newRepeat < 0 && this.sequence.length > 0) { // If the next pattern in the sequence must be called
                    this.setPattern(this.sequence.shift(), false);
                    return;
                } else if (newRepeat < 0 && this.sequence.length === 0) { // If this was the last pattern
                    this._clearPatternInterval();
                    this.pattern = this._emptyPattern();
                    return;
                }

                this.pattern.repeat = newRepeat; // If there is another repeat left
            }
        }

        this.state = this.patterns.patterns[this.pattern.name][this.pattern.index]; // Get the current state
        this.drivers.setState(this.state);
        this.lastInterval = new Date();

        let newIndex;

        // @TODO In a sequence, switching to the same pattern with different speed and no set index causes this to bug out and skip a repeat
        // Calculate the next state index. If the speed is 0, it cycles to the next state when this function is called
        if (this.pattern.speed >= 0) {
            newIndex = this.pattern.index + 1;
            if (newIndex > this.pattern.endIndex) {
                newIndex = this.pattern.startIndex;
                this.newCycle = true;
            }
        } else if (this.pattern.speed < 0) {
            newIndex = this.pattern.index - 1;
            if (newIndex < this.pattern.endIndex) {
                newIndex = this.pattern.startIndex;
                this.newCycle = true;
            }
        }

        this.pattern.index = newIndex;
    }

    /**
     * Validates a patternSet by checking if it exists, all required parameters are present and fills out the optional parameters
     * @param patternSet {Object} The pattern description
     * @param patternSet.name {String} The name of the pattern
     * @param patternSet.speed {Number} The speed of the pattern in states-per-second
     * @param patternSet.index {Number=} The index to start the first repeat with
     * @param patternSet.startIndex {Number=} The index to start the pattern
     * @param patternSet.endIndex {Number=} The final index of the pattern to use before starting over
     * @param patternSet.repeat {Number=-1} How many times the pattern should repeat (-1 means infinitely, 0 means it will do the pattern once)
     * @return {Object|boolean} The pattern object if validated successfully, false otherwise
     * @protected
     */
    _validatePatternSet(patternSet) {
        if (typeof patternSet !== 'object')
            return false;

        if (typeof patternSet.name !== 'string' || typeof patternSet.speed !== 'number')
            return false;

        if(typeof patternSet.index !== 'undefined' && typeof patternSet.index !== 'number')
            return false;

        if (this.patterns.patterns[patternSet.name] === undefined)
            return false;

        let maxUpperIndex = this.patterns.patterns[patternSet.name].length - 1;

        let lowerIndex = patternSet.startIndex;
        let upperIndex = patternSet.endIndex;

        if (lowerIndex === undefined)
            lowerIndex = patternSet.speed >= 0 ? 0 : maxUpperIndex;
        if (upperIndex === undefined)
            upperIndex = patternSet.speed < 0 ? 0 : maxUpperIndex;

        if (patternSet.speed >= 0) {
            patternSet.startIndex = Math.min(lowerIndex, upperIndex, maxUpperIndex);
            patternSet.endIndex = Math.min(maxUpperIndex, Math.max(lowerIndex, upperIndex, 0));
        } else {
            patternSet.startIndex = Math.min(maxUpperIndex, Math.max(lowerIndex, upperIndex, 0));
            patternSet.endIndex = Math.min(lowerIndex, upperIndex, maxUpperIndex);
        }

        if (typeof patternSet.repeat !== 'number')
            patternSet.repeat = -1;

        return patternSet;
    }

    /**
     * Returns an empty pattern object
     * @return {{startIndex: number, endIndex: number, repeat: number, name: string, index: number, speed: number}}
     * @protected
     */
    _emptyPattern() {
        return {
            name: '',
            speed: 0,
            index: 0,
            startIndex: 0,
            endIndex: 0,
            repeat: -1,
        };
    }

    /**
     * Add steps to the sequence. If there is no sequence yet, it will create one
     * @param sequence
     * @protected
     */
    _addSequenceSteps(sequence) {
        for (let i = 0; i < sequence.length; i++) {
            let step = this._validatePatternSet(sequence[i]);

            if (step && (this.sequence.length < parseInt(config.sequences.maxPatterns) || parseInt(config.sequences.maxPatterns) === -1)) {
                this.sequence.push(step);
            }
        }
    }

    /**
     * Set the pattern to display
     * @param source {string} The origin of the call (eg, API, app, ...)
     * @param patternSet {Object} The pattern description
     * @param patternSet.name {String} The name of the pattern
     * @param patternSet.speed {Number} The speed of the pattern in states-per-second
     * @param patternSet.index {Number=} The index to start the first repeat with
     * @param patternSet.startIndex {Number=} The index to start the pattern
     * @param patternSet.endIndex {Number=} The final index of the pattern to use before starting over
     * @param patternSet.repeat {Number=-1} How many times the pattern should repeat (-1 means infinitely, 0 means it will do the pattern once)
     * @param clearSequence {boolean} If the sequence should be cleared
     * @return {boolean} True of the pattern was successfully set, false otherwise
     */
    setPattern(source, patternSet, clearSequence = true) {
        if(!config.patterns.enabled)
            return false;

        patternSet = this._validatePatternSet(patternSet);

        if (!patternSet)
            return false;

        if (!patternSet.index && patternSet.name === this.pattern.name) {
            patternSet.index = parseInt(this.pattern.index);
        } else if(!patternSet.index) {
            patternSet.index = patternSet.startIndex;
        }

        this.pattern = patternSet;

        if (clearSequence === true) {
            this.sequence = [];
        }

        this._setPatternInterval();

        return true;
    }

    /**
     * Set a sequence of patterns to go through
     * @param source {string} The orogin of the call (eg, API, app, ...)
     * @param sequence {Object[]} The sequence of patterns as an array
     * @param sequence[].name {String} The name of the pattern
     * @param sequence[].speed {Number} The speed of the pattern in states-per-second
     * @param sequence[].index {Number=} The index to start the first repeat with
     * @param sequence[].startIndex {Number=} The index to start the pattern
     * @param sequence[].endIndex {Number=} The final index of the pattern to use before starting over
     * @param sequence[].repeat {Number=-1} How many times the pattern should repeat (-1 means infinitely, 0 means it will do the pattern once)
     * @param clear {boolean} True if the sequence needs to start from scratch
     * @return {boolean} True
     */
    setSequence(source, sequence, clear = true) {
        if(!config.sequences.enabled)
            return false;

        if (clear === true || !this.pattern.name) {
            this.sequence = [];
            this._addSequenceSteps(sequence);

            if (this.sequence.length > 0)
                this.setPattern(this.sequence.shift(), false);
        } else {
            this._addSequenceSteps(sequence);
        }

        return true;
    }

    /**
     * Set the LED cube to a specific state.
     * @param source {string} The origin of the call (eg, API, app, ...)
     * This state does not get saved on file, it is a static state shown to the LED cube.
     * @param state {Uint32Array} The desired state
     */
    setState(source, state) {
        this.drivers.setState(state);
        this.state = state;

        this._clearPatternInterval();

        this.pattern = this._emptyPattern();
        this.sequence = [];
        this.lastInterval = null;
    }
}

module.exports = StateManager;