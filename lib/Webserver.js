const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const StateHelper = require('./StateHelper');

const config = require('../config');
const swaggerDocument = require('../resources/API');

/**
 * The public webserver for the LED cube API. From here, the cube is controlled.
 * It serves REST endpoints which are documented in /api/
 */
class Webserver {
    constructor() {
        this.app = null;
        this.patterns = null;
        this.stateManager = null;
    }

    setPatterns(patterns) {
        this.patterns = patterns;
    }

    setStateManager(stateManager) {
        this.stateManager = stateManager;
    }

    run() {
        this._webserver();
        this._endpoints();
    }

    /**
     * Constructs a message that can be sent back to the client
     * @param success {boolean} If the request has been successful
     * @param errors {Array|string=} The message or messages to send
     * @return {string} A JSON string to send to the client
     * @protected
     */
    _returnMessage(success, errors=[]) {
        if(typeof errors === 'string')
            errors = [errors];

        return JSON.stringify({
            success: success,
            errors: errors,
        });
    }

    /**
     * Start the Express web server
     * @protected
     */
    _webserver() {
        this.app = express();
        this.app.listen(config.webserver.port);

        this.app.use(express.static('public_html'));
        this.app.use(bodyParser.json({limit: config.webserver.maxUpload}));
        this.app.use(morgan('combined'));
    }

    /**
     * Defines all endpoints available on the API.
     * @protected
     */
    _endpoints() {
        if(config.webserver.endpoints.GET_api) {
            this.app.use('/api', swaggerUi.serve);
            this.app.get('/api', swaggerUi.setup(swaggerDocument, {
                customSiteTitle: 'LED Cube API',
            }));
        }

        // GET /api/state
        if(config.webserver.endpoints.GET_api_state) {
            this.app.get('/api/state', (req, res) => {
                res.type('application/json');
                res.send(StateHelper.stateUintToJSON(this.stateManager.state));
            });
        }

        // POST /api/state
        if(config.webserver.endpoints.POST_api_state) {
            this.app.post('/api/state', (req, res) => {
                if (req.body.constructor === Array) {
                    this.stateManager.setState('API', StateHelper.stateArrayToUint(req.body));

                    res.type('application/json');
                    res.send(this._returnMessage(true));
                } else {
                    res.status(400);
                    res.type('application/json');
                    res.send(this._returnMessage(false, 'Required parameters missing'));
                }
            });
        }

        // GET /api/pattern
        if(config.webserver.endpoints.GET_api_pattern && config.patterns.enabled) {
            this.app.get('/api/pattern', (req, res) => {
                res.type('application/json');
                res.send(JSON.stringify(this.stateManager.pattern));
            });
        }

        // POST /api/pattern
        if(config.webserver.endpoints.POST_api_pattern && config.patterns.enabled) {
            this.app.post('/api/pattern', (req, res) => {
                let pattern = this.stateManager.setPattern('API', req.body);

                if (pattern) {
                    res.type('application/json');
                    res.send(JSON.stringify(this.stateManager.pattern));
                } else {
                    res.status(400);
                    res.type('application/json');
                    res.send(this._returnMessage(false, 'Required parameters missing'));
                }
            });
        }

        // GET /api/patterns
        if(config.webserver.endpoints.GET_api_patterns && config.patterns.enabled) {
            this.app.get('/api/patterns', (req, res) => {
                let patterns = [];

                for (let patternName in this.patterns.patterns) {
                    if (this.patterns.patterns.hasOwnProperty(patternName)) {
                        patterns.push({
                            name: patternName,
                            pattern: StateHelper.patternUintArrayToArray(this.patterns.patterns[patternName]),
                        });
                    }
                }

                res.type('application/json');
                res.send(JSON.stringify(patterns));
            });
        }

        // POST /api/patterns
        if(config.webserver.endpoints.POST_api_patterns && config.patterns.enabled) {
            this.app.post('/api/patterns', (req, res) => {
                if (req.body.name && req.body.pattern && req.body.pattern.constructor === Array) {
                    let pattern = StateHelper.patternArrayToUintArray(req.body.pattern);

                    this.patterns.addPattern(req.body.name, pattern);

                    res.type('application/json');
                    res.send(this._returnMessage(true));
                } else {
                    res.status(400);
                    res.type('application/json');
                    res.send(this._returnMessage(false, 'Required parameters missing'));
                }
            });
        }

        // GET /patterns/:name
        if(config.webserver.endpoints.GET_api_patterns_name && config.patterns.enabled) {
            this.app.get('/api/patterns/:name', (req, res) => {
                let pattern = {};

                if (this.patterns.patterns[req.params.name]) {
                    pattern.name = req.params.name;
                    pattern.pattern = StateHelper.patternUintArrayToArray(this.patterns.patterns[req.params.name]);
                }

                res.type('application/json');
                res.send(JSON.stringify(pattern));
            });
        }

        // DELETE /api/pattern/:name
        if(config.webserver.endpoints.DELETE_api_patterns_name && config.patterns.enabled) {
            this.app.delete('/api/patterns/:name', (req, res) => {
                if (req.params.name) {
                    this.patterns.deletePattern(req.params.name.toString());

                    res.type('application/json');
                    res.send(this._returnMessage(true));
                } else {
                    res.status(400);
                    res.type('application/json');
                    res.send(this._returnMessage(false, 'Required parameters missing'));
                }
            });
        }

        // GET /api/sequence
        if(config.webserver.endpoints.GET_api_sequence && config.sequences.enabled) {
            this.app.get('/api/sequence', (req, res) => {
                res.send(JSON.stringify(this.stateManager.sequence));
            });
        }

        // POST /api/sequence
        if(config.webserver.endpoints.POST_api_sequence && config.sequences.enabled) {
            this.app.post('/api/sequence', (req, res) => {
                if (req.body.constructor === Array) {
                    this.stateManager.setSequence('API', req.body);

                    res.type('application/json');
                    res.send(this._returnMessage(true));
                } else {
                    res.status(400);
                    res.type('application/json');
                    res.send(this._returnMessage(false, 'Input must be an array'));
                }
            });
        }

        // PUT /api/sequence
        if(config.webserver.endpoints.PUT_api_sequence && config.sequences.enabled) {
            this.app.put('/api/sequence', (req, res) => {
                if (req.body.constructor === Array) {
                    this.stateManager.setSequence('API', req.body, false);

                    res.type('application/json');
                    res.send(this._returnMessage(true));
                } else {
                    res.status(400);
                    res.type('application/json');
                    res.send(this._returnMessage(false, 'Input must be an array'));
                }
            });
        }

        // POST /api/clear
        if(config.webserver.endpoints.POST_api_clear) {
            this.app.post('/api/clear', (req, res) => {
                this.stateManager.setState('API', new Uint32Array(128));

                res.type('application/json');
                res.send(this._returnMessage(true));
            });
        }
    }
}

module.exports = Webserver;