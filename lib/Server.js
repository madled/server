const async = require('async');
const Drivers = require('./Drivers');
const Patterns = require('./Patterns');
const StateManager = require('./StateManager');
const Webserver = require('./Webserver');
const config = require('../config');

/**
 * Main class of the LED cube server.
 * This handles all components needed to run the LED server and corresponding API.
 *
 */
class Server {
    constructor() {
        this.drivers = null;
        this.patterns = null;
        this.stateManager = null;
        this.webserver = null;
    }

    /**
     * This method is the only method being called from the outside.
     * This starts all components.
     */
    run() {
        async.series([
            (callback) => {
                this._startDrivers(callback);
            },
            (callback) => {
                this._startPatterns(callback);
            },
            (callback) => {
                this._startStateManager(callback);
            },
            (callback) => {
                this._startServer(callback);
            },
        ], (err) => {
            if(err)
                console.error(err);

            console.log('Cube server ready to square!');
            console.log('');
            console.log('   +------------+');
            console.log('  /            /|');
            console.log(' /            / |');
            console.log('+------------+  |');
            console.log('|            |  |');
            console.log('|            |  +');
            console.log('|            | /');
            console.log('|            |/');
            console.log('+------------+');
            console.log('');
        });
    }

    /**
     * Starts the drivers manager.
     * @param callback {function=} Callback gets called when all drivers are loaded
     * @protected
     */
    _startDrivers(callback) {
        this.drivers = new Drivers();

        this.drivers.loadDrivers(() => {
            if (callback)
                callback();
        });
    }

    /**
     * Starts the patterns manager.
     * @param callback {function=} Callback gets called when the patterns are loaded
     * @protected
     */
    _startPatterns(callback) {
        if(config.patterns.enabled) {
            this.patterns = new Patterns();
            this.patterns.loadPatterns(() => {
                if (callback)
                    callback();
            });
        }
        else {
            if (callback)
                callback();
        }
    }

    /**
     * Starts the state manager.
     * @param callback {function=} Callback gets called when the state manager is loaded
     * @protected
     */
    _startStateManager(callback) {
        this.stateManager = new StateManager(this.drivers, this.patterns);

        if (callback)
            callback();
    }

    /**
     * Starts the webserver for the API.
     * @param callback {function=} Callback gets called when the webserver is running
     * @protected
     */
    _startServer(callback) {
        this.webserver = new Webserver();
        this.webserver.setPatterns(this.patterns);
        this.webserver.setStateManager(this.stateManager);
        this.webserver.run();

        if (callback)
            callback();
    }
}

module.exports = Server;