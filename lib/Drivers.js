const async = require('async');
const fs = require('fs');
const path = require('path');

const config = require('../config');

/**
 * The "factory" class for all cube drivers.
 * These drivers are automatically detected by the Drivers class.
 * It looks in the drivers/ folder and imports all files ending in .js.
 * These drivers are imported. They should all extend the master Driver class.
 * It tests if the driver can run with a self test the driver performs.
 */
class Drivers {
    constructor() {
        this.whitelist = config.drivers.load || 'auto';
        this.drivers = [];
    }

    /**
     * Detect and load all drivers in the drivers/ folder.
     * @param callback {function=} Gets called after all drivers are loaded
     */
    loadDrivers(callback) {
        let driversPath = path.join(__dirname, 'drivers');

        fs.readdir(driversPath, (err, files) => {
            async.each(files, (file, eachCallback) => {
                if (file.endsWith('.js') && (this.whitelist === 'auto' || this.whitelist.indexOf(file.replace('.js', '')) !== -1)) {
                    file = path.join(driversPath, file);

                    try {
                        if (fs.lstatSync(file).isFile()) { // Only continue if the file exists
                            let Driver = require(file);

                            console.info(`Found driver ${Driver.name}`);

                            this._addDriver(Driver, () => {
                                eachCallback(); // Driver loaded, continue to next
                            });
                        }
                        else {
                            eachCallback(); // It's not a file, continue to next
                        }
                    }
                    catch (e) {
                        console.error(`Error while processing driver file ${path.basename(file)}`);
                        console.error(e);
                    }
                }
                else {
                    eachCallback(); // File not a valid JavaScript file, continue to next
                }
            }, err => {
                if (err) {
                    console.error(err);
                }

                if (callback)
                    callback();
            });
        });
    }

    /**
     * Loads a single driver
     * @param Driver {Driver} The driver class to load
     * @param callback {function=} Gets called after the driver is loaded
     * @protected
     */
    _addDriver(Driver, callback) {
        let driver = new Driver();

        driver.selfTest((result) => {
            if (result){
                driver.init(() => {
                    this.drivers.push(driver);
                    console.info(`Added driver ${Driver.name}`);
                    if(callback)
                        callback();
                });
            } else {
                if(callback)
                    callback();
            }
        });
    }

    /**
     * Sets the LED state of all LED cubes. This broadcasts the desired state to all available drivers.
     * @param array {Uint32Array} The array which represents the LED state
     */
    setState(array) {
        async.each(this.drivers, (driver, callback) => {
            driver.setState(array);

            callback();
        });
    }
}

module.exports = Drivers;