const async = require('async');
const fs = require('fs');
const path = require('path');

const config = require('../config');

/**
 * Patterns is the manager for all patterns.
 * It stores the current list of available patterns.
 * Also, it allows you to create new patterns and delete existing ones.
 * If you modify a pattern, it gets saved into a .bin file inside patterns/.
 */
class Patterns {
    constructor() {
        this.whitelist = config.patterns.load || 'auto';
        this.patterns = {};
    }

    /**
     * Loads all available patterns from the patterns/ folder.
     * @param callback {function=} Callback gets called when all patterns are loaded
     */
    loadPatterns(callback) {
        let patternsPath = path.join(__dirname, '../patterns');

        fs.readdir(patternsPath, (err, files) => {
            async.each(files, (file, eachCallback) => {
                if (file.endsWith('.bin') && (this.whitelist === 'auto' || this.whitelist.indexOf(file.replace('.bin', '')) !== -1)) {
                    file = path.join(patternsPath, file);

                    try {
                        if (fs.lstatSync(file).isFile()) {

                            console.info(`Found pattern ${path.basename(file)}`);

                            this._addPatternFile(file, () => {
                                eachCallback(); // Pattern loaded, continue to next
                            });
                        } else {
                            eachCallback(); // Pattern not a file, continue to next
                        }
                    } catch (e) {
                        console.error(`Error while processing pattern file ${path.basename(file)}`);
                        console.error(e);
                    }
                } else {
                    eachCallback(); // File not a valid pattern, continue to next
                }
            }, err => {
                if (err) {
                    console.error(err);
                }

                if (callback)
                    callback();
            });
        });
    }

    /**
     * Loads a single pattern.
     * A pattern is defined in a .bin file. This file contains 128 32-bit bytes for each state (or layer) the pattern has.
     * The 32-bit byte is defined as follows: bits 0-7, bits 8-15, bits 16-23, bits 24-31
     * @param file {string} the absolute path of the file
     * @param callback {function=} Callback gets called when the file is loaded
     * @protected
     */
    _addPatternFile(file, callback) {
        fs.readFile(file, (err, data) => {
            if (err) {
                console.error(err);

                if (callback)
                    callback();
            }

            let pattern = [];

            let state = new Uint32Array(128);

            // Loop through each 8-bit byte and reconstruct the 32-bit bytes
            let byteIndex = 0;
            for (let bufferIndex = 0; bufferIndex < data.length; bufferIndex = bufferIndex + 4) {
                if (bufferIndex % 512 === 0 && bufferIndex !== 0) { // Every 128 32-bit bytes (512 8-bit bytes), a new state starts
                    pattern.push(state);
                    state = new Uint32Array(128);
                    byteIndex = 0;
                }

                let byte = 0;

                byte = byte + (data[bufferIndex]);
                byte = byte + (data[bufferIndex + 1] << 8);
                byte = byte + (data[bufferIndex + 2] << 16);
                byte = byte + ((data[bufferIndex + 3] << 24) >>> 0);

                state[byteIndex] = byte;

                byteIndex++;
            }
            pattern.push(state);

            this.patterns[path.basename(file, '.bin')] = pattern;

            if (callback)
                callback();
        });
    }

    /**
     * Add a pattern to the system.
     * These patterns should come as an array of Uint32Array's, each 128 bytes in size.
     * The pattern gets saved and is immediately available under it's name.
     * If a pattern with the same name already exists, it overwrites that pattern.
     * @param name {string} Name of the pattern
     * @param pattern {array} An array of Uint23Array's
     */
    addPattern(name, pattern) {
        if(this.whitelist !== 'auto' && this.whitelist.indexOf(name.replace('.bin', '')) === -1)
            return false;

        this.patterns[name] = pattern;

        let savePattern = new Uint32Array(pattern.length * 128);

        for (let i = 0; i < pattern.length; i++) {
            savePattern.set(pattern[i], i * 128);
        }

        fs.writeFile(`./patterns/${name}.bin`, savePattern, "binary", function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log(`Saved pattern ${name}`);
            }
        });
    }

    /**
     * Delete an existing pattern from the patterns list.
     * This also deletes the file.
     * @param name {string} The name of the pattern
     */
    deletePattern(name) {
        if(this.patterns[name]) {
            delete this.patterns[name];

            let filepath = path.join(__dirname, '../patterns', `${name}.bin`);

            if (fs.lstatSync(filepath).isFile()) {
                fs.unlinkSync(filepath);
            }
        }
    }
}

module.exports = Patterns;