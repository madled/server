const Driver = require('../Driver');
const exec = require('child_process').exec;

const config = require('../../config');

/**
 * VirtualDriver is a driver for the LED cube.
 * This driver controls a virtual representation of the cube, available as a web page.
 * Communication between the driver and the cube is performed via a WebSocket connection.
 * This WebSocket is on a separate port to the API and runs completely separate from the webserver.
 * States are broadcast as an array of numbers.
 */
class I2CDriver extends Driver {
    constructor() {
        super();

        this.i2cLib = null;
        this.i2c = null;
    }

    /**
     * Initializes the driver async
     * @param callback {function=} The callback function that gets called after the init is complete
     * @return void
     */
    init(callback) {

        if (callback)
            callback();
    }

    /**
     * Performs a self-test to check if the driver can run.
     * @param callback {function} Called when self-test is complete
     * @return {boolean} True of the self test is successful, false otherwise
     */
    selfTest(callback) {
        exec('npm ls i2c-bus', (error, stdout, stderr) => {
            let installed = stdout.toString().indexOf('UNMET');

            if (installed === -1) {
                try {
                    let i2cLib = require('i2c-bus');

                    if (i2cLib) {
                        this.i2c = i2cLib.open(config.drivers.i2c.bus, (err) => {
                            if (err) {
                                console.error('Error while opening I2C:');
                                console.error(err);
                                callback(false);
                            } else
                                callback(true);
                        });
                    } else {
                        console.error('i2c-bus could not be required');
                        callback(false);
                    }
                } catch (e) {
                    console.error('Error while starting i2c');
                    console.error(e);
                    callback(false);
                }
            } else {
                console.error('i2c-bus not installed');
                callback(false);
            }
        });
    }

    /**
     * Convert a Uint32Array(128) into a Buffer(512)
     * @param bytes {Uint32Array}
     * @protected
     */
    _bytesToBuffer(bytes) {
        let output = Buffer.alloc(512);

        for (let index = 0; index < bytes.byteLength; index++) {
            let byte = bytes[index];

            output[index * 4 + 3] = (byte & 0xff000000) >>> 24;
            output[index * 4 + 2] = (byte & 0x00ff0000) >>> 16;
            output[index * 4 + 1] = (byte & 0x0000ff00) >>> 8;
            output[index * 4] = (byte & 0x000000ff);
        }

        return output;
    }

    /**
     * Split all Buffers into a format that can be sent to the Arduinos directly
     * @param buffer Buffer A single buffer containing all bytes
     * @returns [[{Buffer}]] An array of 4 items, 1 for each Arduino. Each item has an array of 17 buffers
     * @protected
     */
    _splitBytes(buffer) {
        let arduinos = [];

        for (let arduinoIndex = 0; arduinoIndex < 4; arduinoIndex++) {
            arduinos.push([]);

            for (let byteIndex = 0; byteIndex < 8; byteIndex++) {
                arduinos[arduinoIndex].push(Buffer.alloc(17));
                arduinos[arduinoIndex][byteIndex][0] = byteIndex;
            }
        }

        for (let index = 0; index < buffer.byteLength; index++) {
            let arduinoIndex = Math.floor((index % 32) / 8);
            let counter = Math.floor((index % 64) / 32);
            let byteIndex = 1 + (counter * 8) + (index % 8);
            let byteArray = Math.floor(index / 64);

            arduinos[arduinoIndex][byteArray][byteIndex] = buffer[index];
        }

        return arduinos;
    }

    /**
     * Send the raw Buffer data to all available Arduinos using I2C
     * @param array [[{Buffer}]] An array of 4 items, 1 for each Arduino. Each item has an array of 17 buffers
     * @protected
     */
    _sendState(array) {
        // Loop through all (4) Arduinos
        for (let i = 0; i < 4; i++) {
            let arduino = array[i];
            let address = config.drivers.i2c.addresses[i];

            if (address !== null) {
                // Loop through all (8) buffers
                for (let j = 0; j < arduino.length; j++) {
                    try {
                        this.i2c.i2cWriteSync(4, 17, arduino[j]); // First byte defines the block, other 16 bytes draw the block
                    } catch (e) {
                        console.error('Error while writing I2C buffer:');
                        console.error(e.message);
                    }
                }
            }
        }
    }

    /**
     * Sets the cube state. It receives the full LED state as a Unit32Array, which defines the cube from top to bottom.
     * @param array {Uint32Array} 128-bytes array of the cube state
     * @return void
     */
    setState(array) {
        let buffer = this._bytesToBuffer(array);
        let sendArray = this._splitBytes(buffer);

        this._sendState(sendArray);
    }
}

module.exports = I2CDriver;