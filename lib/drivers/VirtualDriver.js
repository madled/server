const Driver = require('../Driver');
const express = require('express');
const socketio = require('socket.io');
const CronJob = require('cron').CronJob;
const http = require('http');
const port = 10123;

/**
 * VirtualDriver is a driver for the LED cube.
 * This driver controls a virtual representation of the cube, available as a web page.
 * Communication between the driver and the cube is performed via a WebSocket connection.
 * This WebSocket is on a separate port to the API and runs completely separate from the webserver.
 * States are broadcast as an array of numbers.
 */
class VirtualDriver extends Driver {
    constructor() {
        super();

        this.state = new Uint32Array(128);
    }

    /**
     * Initializes the driver async
     * @param callback {function=} The callback function that gets called after the init is complete
     * @return void
     */
    init(callback) {
        this.app = express();
        this.server = http.Server(this.app);
        this.server.listen(port);
        this.socket = socketio(this.server);

        // Send a heartbeat every 30 seconds
        new CronJob('*/30 * * * * *', () => {
            this._broadcastHeartbeat();
        }, null, true, 'Europe/Amsterdam');

        this._socketHandler();

        if (callback)
            callback();
    }

    /**
     * Performs a self-test to check if the driver can run.
     * @param callback {function} Called when self-test is complete
     * @return {boolean} True of the self test is successful, false otherwise
     */
    selfTest(callback) {
        if(callback)
            callback(true);
    }

    /**
     * Broadcasts a heartbeat to all connected sockets
     * @protected
     */
    _broadcastHeartbeat() {
        this.socket.sockets.emit('broadcast', {
            type: 'heartbeat',
            data: new Date(),
        });
    }

    /**
     * Handles a new incoming socket connection. It receives a pong to let it know it connected successfully
     * @protected
     */
    _socketHandler() {
        this.socket.on('connection', (socket) => {
            socket.emit('hello', { hello: 'world' });
            this._sendState();
        });
    }

    /**
     * Sets the cube state. It receives the full LED state as a Unit32Array, which defines the cube from top to bottom.
     * @param array {Uint32Array} 128-bytes array of the cube state
     * @return void
     */
    setState(array) {
        this.state = array;
        this._sendState();
    }

    /**
     * Broadcasts a new state to all connected sockets
     * @protected
     */
    _sendState() {
        this.socket.sockets.emit('broadcast', {
            type: 'state',
            data: this.state,
        });
    }
}

module.exports = VirtualDriver;