class Driver {
    constructor() {

    }

    /**
     * Initializes the driver async
     * @param callback {function=} The callback function that gets called after the init is complete
     * @return void
     */
    init(callback) {
        if(callback)
            callback();
    }

    /**
     * Performs a self-test to check if the driver can run.
     * @param callback {function} Called when self-test is complete
     * @return {boolean} True of the self test is successful, false otherwise
     */
    selfTest(callback) {
        if(callback)
            callback(false);
    }

    /**
     * Sets the cube state. It receives the full LED state as a Unit32Array, which defines the cube from top to bottom.
     * @param array {Uint32Array} 128-bytes array of the cube state
     * @return void
     */
    setState(array) {

    }
}

module.exports = Driver;