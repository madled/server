FROM node:lts

RUN curl -sSL https://gitlab.com/madled/server/-/archive/master/server-master.tar.gz \
		| tar -v -C / -xz
RUN mv /server-master /server
WORKDIR /server
RUN cp config.js.example config.js
RUN chmod +x ./build.sh
RUN ./build.sh

WORKDIR /server
CMD ["node", "./cubeAPI.js"]