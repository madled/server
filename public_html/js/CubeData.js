/**
 * Connects the virtual cube to the virtual cube driver, via it's WebSocket connection.
 * This class receives new incoming states, and feeds it to the cube
 */
class CubeData {
    constructor(cube) {
        this.cube = cube;
        this.socket = null;
    }

    run() {
        let protocol = 'http';
        if (location.protocol === 'https:') {
            protocol += 's';
        }

        let URL = `${protocol}://${window.location.hostname}:10123`;

        this.socket = io(URL);
        this.socket.on('broadcast', function (data) {
            if (data.type === 'state') {
                cube.setData(data.data);
            }
        });
    }
}