/**
 * Cube uses Three.JS to create a virtual cube for testing purposes.
 * The size of the cube is defined in the constructor, but the entire backend counts on it being a 16x16x16 cube.
 */
class Cube {
    constructor(width = 1024, height = 768, container = null) {
        this.width = width;
        this.height = height;
        this.container = container;

        // Define the cube's physical size
        this.cube = {
            side: 16,
            offset: 5,
            LEDSize: 0.5,
        };
        this.calcData = {}; // Gets some extra data, derived from this.cube

        this.scene = null;
        this.camera = null;
        this.controls = null;
        this.renderer = null;
        this.LED = {
            geometry: null,
            materials: {
                off: null,
                on: null,
            }
        };

        // "theme" for the LED
        this.materialDefs = {};
        this.materialDefs.off = {
            color: 0x000000,
            specular: 0x666666,
            emissive: 0x0000ff,
            shininess: 10,
            opacity: 0.2,
            transparent: true
        };
        this.materialDefs.on = {
            color: 0x000000,
            specular: 0x666666,
            emissive: 0x0000ff,
            shininess: 10,
            opacity: 1,
            transparent: true
        };

        this.LEDs = [];

        this.orbit = {
            enabled: true,
            theta: 0,
            radius: 100,
        };
    }

    getQueryStringParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    _parseQueryString() {
        let orbitEnabled = this.getQueryStringParameterByName('orbit');
        if (orbitEnabled === 'false') {
            this.orbit.enabled = false;
        }
    }

    /**
     * Create calculated data derived from this.cube
     * @protected
     */
    _createCalcData() {
        this.calcData.cube = {};
        this.calcData.cube.size = this.cube.side * this.cube.offset;
    }

    /**
     * Create the Three.JS scene
     * @protected
     */
    _createScene() {
        this.scene = new THREE.Scene();
    }

    /**
     * Create the Three.JS renderer
     * @protected
     */
    _createRenderer() {
        this.renderer = new THREE.WebGLRenderer({alpha: true});
        this.renderer.setClearColor(0x000000, 0);
        this.renderer.setSize(this.width, this.height);
        this.container.appendChild(this.renderer.domElement);
    }

    /**
     * Create the Three.JS camera
     * @protected
     */
    _createCamera() {
        this.camera = new THREE.PerspectiveCamera(75, this.width / this.height, 0.1, 1000);
        this.camera.position.x = -90;
        this.camera.position.y = 55;
        this.camera.position.z = 70;

        this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        this.controls.minDistance = 1;
        this.controls.maxDistance = 500;
        this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.05;
        this.controls.update();
    }

    /**
     * Create the LED definition (shape and textures)
     * @protected
     */
    _createLED() {
        this.LED.geometry = new THREE.SphereGeometry(this.cube.LEDSize, 8, 8);
        this.LED.materials.off = new THREE.MeshPhongMaterial(this.materialDefs.off);
        this.LED.materials.on = new THREE.MeshPhongMaterial(this.materialDefs.on);
    }

    /**
     * Create the cube itself. The LEDs are numbered 0 - 4095 from top left, to bottom right
     * @protected
     */
    _createCube() {
        for (let y = this.calcData.cube.size; y > 0; y = y - this.cube.offset) {
            for (let x = 0; x < this.calcData.cube.size; x = x + this.cube.offset) {
                for (let z = 0; z < this.calcData.cube.size; z = z + this.cube.offset) {
                    let sphere = new THREE.Mesh(this.LED.geometry, this.LED.materials.off);
                    sphere.position.x = x - (this.calcData.cube.size / 2);
                    sphere.position.z = z - (this.calcData.cube.size / 2);
                    sphere.position.y = y - (this.calcData.cube.size / 2);
                    this.scene.add(sphere);

                    this.LEDs.push(sphere);
                }
            }
        }
    }

    _orbitCamera() {
        setInterval(() => {
            if (this.orbit.enabled) {
                this.orbit.theta = Date.now() / 80 % 360;
                this.camera.position.x = this.orbit.radius * Math.sin(THREE.Math.degToRad(this.orbit.theta));
                this.camera.position.z = this.orbit.radius * Math.cos(THREE.Math.degToRad(this.orbit.theta));
            }
        }, 10);
    }

    /**
     * Animate the LED updates and camera movements
     * @protected
     */
    _animate() {
        let animate = function () {
            requestAnimationFrame(animate);

            this.controls.update();

            this.renderer.render(this.scene, this.camera);
        }.bind(this);

        animate();
    }

    /**
     * Run the virtual LED cube.
     * This bootstraps all internal methods to setup the cube.
     */
    run() {
        this._parseQueryString();
        this._createCalcData();
        this._createScene();
        this._createRenderer();
        this._createCamera();
        this._createLED();
        this._createCube();
        this._orbitCamera();
        this._animate();
    }

    /**
     * Resizes the area to draw on
     * @param width {number}
     * @param height {number}
     */
    resize(width, height) {
        this.width = width;
        this.height = height;

        this.camera.aspect = this.width / this.height;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(this.width, this.height);
    }

    /**
     * Sets the LED state for each LED in the cube.
     * This requires a Uint32Array(128). Each bit represents an LED in the cube.
     * If the bit is 1, the light is turned on. If the bit is 0, the light gets turned off.
     * @param unitArray {Object} - The array of 32-bit bytes representing all LEDs
     */
    setData(unitArray) {
        let index = 0;
        for (let key in unitArray) {
            for (let j = 0; j < 32; j++) {
                if (unitArray[key] & 1 << j)
                    this.LEDs[index].material = this.LED.materials.on;
                else
                    this.LEDs[index].material = this.LED.materials.off;

                index++;
            }
        }
    }
}