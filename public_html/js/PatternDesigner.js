class PatternDesigner {
    constructor() {
        this.elements = {
            patternControls: document.getElementById('pattern-controls'),
        };

        this.pattern = []; // A pattern consists of 1 or more states

        this.currentState = 0; // The current state
        this.currentLayer = 0; // The current layer within a state (as we only show the control for 1 layer at a time; a state has 16 layers)

        this.requestStateUpdate = true;
    }

    run() {
        this._addState();
        this._drawGrid();
        this._listenPatternControls();
        this._listenNavControls();
        this._listenStateUpdate();
    }

    _listenPatternControls() {
        $(document).on('change', '#pattern-controls input', (event) => {
            let dataType = $(event.target).attr('data-type');

            if (dataType === 'bit') {
                this._changeBit(
                    this.currentState,
                    $(event.target).attr('data-byte'),
                    $(event.target).attr('data-bit'),
                    event.target.checked);

                if (!event.target.checked) {
                    $(event.target).parent().parent().find('th input').prop('checked', false);
                    $(`#pattern-controls th input[data-column=${$(event.target).attr('data-column')}]`).prop('checked', false);
                    $(`#pattern-controls th input[data-type=layer]`).prop('checked', false);
                }
            }

            if (dataType === 'row') {
                $(event.target).parent().parent().find('td input').each((i, element) => {
                    if (event.target.checked !== element.checked) {
                        this._changeBit(
                            this.currentState,
                            $(element).attr('data-byte'),
                            $(element).attr('data-bit'),
                            event.target.checked);
                    }

                    $(element).prop('checked', event.target.checked);
                });
            }

            if (dataType === 'col') {
                $(`#pattern-controls td input[data-column=${$(event.target).attr('data-column')}]`).each((i, element) => {
                    if (event.target.checked !== element.checked) {
                        this._changeBit(
                            this.currentState,
                            $(element).attr('data-byte'),
                            $(element).attr('data-bit'),
                            event.target.checked);
                    }

                    $(element).prop('checked', event.target.checked);
                });
            }

            if (dataType === 'layer') {
                $(`#pattern-controls td input`).each((i, element) => {
                    if (event.target.checked !== element.checked) {
                        this._changeBit(
                            this.currentState,
                            $(element).attr('data-byte'),
                            $(element).attr('data-bit'),
                            event.target.checked);
                    }

                    $(element).prop('checked', event.target.checked);
                });
            }
        });
    }

    _listenNavControls() {
        $('#nav-controls-state-up').click((event) => {
            event.preventDefault();
            this.currentState = Math.max(this.currentState - 1, 0);

            this._drawGrid();
            this._updateNavControls();

            this.requestStateUpdate = true;
        });
        $('#nav-controls-state-down').click((event) => {
            event.preventDefault();
            this.currentState++;
            if (this.pattern.length <= this.currentState) {
                this._addState();
            }

            this._drawGrid();
            this._updateNavControls();

            this.requestStateUpdate = true;
        });


        $('#nav-controls-layer-up').click((event) => {
            event.preventDefault();
            this.currentLayer = Math.max(this.currentLayer - 1, 0);

            this._drawGrid();
            this._updateNavControls();
        });
        $('#nav-controls-layer-down').click((event) => {
            event.preventDefault();
            this.currentLayer = Math.min(this.currentLayer + 1, 15);

            this._drawGrid();
            this._updateNavControls();
        });

        $('#nav-controls-load').click((event) => {
            event.preventDefault();
            let name = $('#nav-controls-name').val();

            if (name) {
                $.getJSON(`/api/patterns/${name}`, (data) => {
                    this.pattern = [];
                    this.currentState = 0;
                    this.currentLayer = 0;

                    for (let i = 0; i < data.pattern.length; i++) {
                        this.pattern.push(new Uint32Array(data.pattern[i]));
                    }

                    this._drawGrid();
                    this._updateNavControls();
                    this.requestStateUpdate = true;
                });
            }
        });

        $('#nav-controls-save').click((event) => {
            event.preventDefault();
            let name = $('#nav-controls-name').val();

            if (name) {
                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: '/api/patterns',
                    data: JSON.stringify({
                        name,
                        pattern: this.pattern,
                    }),
                });
            }
        });

        $('#nav-controls-layer-duplicate-prev').click((event) => {
            event.preventDefault();
            if (this.currentLayer > 0) {
                for(let i = 0;i<8;i++) {
                    this.pattern[this.currentState][(this.currentLayer*8)+i] = this.pattern[this.currentState][((this.currentLayer*8)+i)-8];
                }

                this._drawGrid();
                this.requestStateUpdate = true;
            }
        });

        $('#nav-controls-layer-duplicate-next').click((event) => {
            event.preventDefault();
            if (this.currentLayer < 15) {
                for(let i = 0;i<8;i++) {
                    this.pattern[this.currentState][(this.currentLayer*8)+i] = this.pattern[this.currentState][((this.currentLayer*8)+i)+8];
                }

                this._drawGrid();
                this.requestStateUpdate = true;
            }
        });
    }

    _updateNavControls() {
        $('#nav-controls-state-indicator').html(`${this.currentState + 1}/${this.pattern.length}`);
        if (this.currentState === 0)
            $('#nav-controls-state-up').addClass('disabled');
        else
            $('#nav-controls-state-up').removeClass('disabled');

        $('#nav-controls-layer-indicator').html(`${this.currentLayer + 1}/16`);
        if (this.currentLayer === 0) {
            $('#nav-controls-layer-up').addClass('disabled');
        } else if (this.currentLayer === 15) {
            $('#nav-controls-layer-down').addClass('disabled');
        } else {
            $('#nav-controls-layer-down').removeClass('disabled');
            $('#nav-controls-layer-up').removeClass('disabled');
        }
    }

    _listenStateUpdate() {
        setInterval(() => {
            if (this.requestStateUpdate) {
                this._updateCube();
                this.requestStateUpdate = false;
            }
        }, 100);
    }

    _drawGrid() {
        let html = '';

        let headerHtml = `<th>${this._generateCheckbox('layer')}</th>`;
        for (let col = 0; col < 16; col++) {
            headerHtml += `<th>${this._generateCheckbox('col', {col})}</th>`;
        }
        html += `<tr>${headerHtml}</tr>`;

        for (let row = 0; row < 16; row++) {
            let rowHtml = `<th>${this._generateCheckbox('row', {row})}</th>`;

            for (let col = 0; col < 16; col++) {
                let byte = Math.floor(col / 2) + (this.currentLayer * 8);
                let bit = col % 2 === 0 ? row : 16 + row;

                rowHtml += `<td>${this._generateCheckbox('bit', {byte, bit, col, row})}`;
            }

            html += `<tr>${rowHtml}</tr>`;
        }

        this.elements.patternControls.innerHTML = `<table id="pattern-controls-table" class="table table-bordered">${html}</table>`;
    }

    _generateCheckbox(type, data) {
        let attributes = '';
        let checked = '';

        if (type === 'col') {
            attributes = ` data-column="${data.col}"`;
        } else if (type === 'row') {
            attributes = ` data-row="${data.row}"`;
        } else if (type === 'bit') {
            attributes = ` data-byte="${data.byte}" data-bit="${data.bit}" data-column="${data.col}" data-row="${data.row}"`;
            if (this.pattern[this.currentState][data.byte] & 1 << data.bit) {
                checked = ' checked';
            }
        }

        return `<input data-type="${type}"${attributes} type="checkbox"${checked}>`;
    }

    /**
     * Changes the bit for a state
     * @param state {number}
     * @param byte {number}
     * @param bit {number}
     * @param enabled {boolean} If enabled is true, the bit will be set
     * @private
     */
    _changeBit(state, byte, bit, enabled) {
        if (this.pattern.length <= state) { // If the state is new
            for (let i = this.pattern.length - 1; i < state; i++) {
                this._addState();
            }
        }

        let intValue = Math.pow(2, bit);

        if (enabled) {
            this.pattern[state][byte] |= (1 << bit);
        } else {
            this.pattern[state][byte] &= ~(1 << bit);
        }

        this.requestStateUpdate = true;
    }

    _addState() {
        this.pattern.push(new Uint32Array(128));
    }

    /**
     * Calculates the integer value of a byte within a state
     * @param state
     * @param byte
     */
    calculateByte(state, byte) {

    }

    /**
     * Gets called after each update to the checkbox controls
     * @private
     */
    _updateCube() {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: '/api/state',
            data: '[' + this.pattern[this.currentState].toString() + ']',
        });
    }
}