# LED cube server

## Introduction

The LED cube server is a piece of software designed to run a 16x16x16 single-colour LED cube.

Please don't ask why we decided to build one, I honestly can't remember.

In case you decide this is a good idea, this software drives the LEDs via I2C. Usually, this software would be run in a Raspberry Pi which is connected to four Arduino Mega's via I2C.

## Installation

### Manual

```
1. git clone https://gitlab.com/phochs/ledcube.git
2. cd ledcube
3. chmod +x build.sh
4. ./build.sh
```
After that's done, run `node cubeAPI.js`.

### Docker

1. Create a directory to work in:  
   `mkdir led-cube && cd led-cube`
1. Download the Dockerfile from the repository:  
   `wget https://gitlab.com/madled/server/raw/master/Dockerfile`
1. Build the Dockerfile using:  
   `docker build --no-cache -t name .`
1. Add it as a container, and pass ports 80 and 10123

## Usage

The previous step starts the cube server, which in turn does a lot of stuff. We care about the following things:
1. It provides a web API where you can control the LED cube
1. It provides two drivers:
    * VirtualDriver: controls the virtual LED cube provided at the root of the web API. Useful for testing
    * ArduinoDriver: Controls the physical LED cube through I2C
1. It provides patterns. You can use the existing patterns, or add new cool ones. Please do create a merge request when you've created some cool ones!